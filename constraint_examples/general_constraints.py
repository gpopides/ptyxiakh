from typing import List, Tuple
from ortools.sat.python import cp_model


class ConstraintExample:
    def __init__(self):
        self.model = cp_model.CpModel()

    def create_integer_variable(self, lower_bound, upper_bound, name):
        """Creates an Integer variable."""
        return self.model.NewIntVar(lower_bound, upper_bound, name)

    def create_interval_variable(self, start, size, end, name):
        """Creates an Interval variable."""
        return self.model.NewIntervalVar(start, size, end, name)

    def add_linear_constraint(self, bounds, linear_lb, linear_ub):
        """
        Adds linear constraint.
        Args:
            bounds: a list/tuple of tuples.  Each tuple contains
            lower and upper bound.
            linear_lb: integer
            linear_ub: integer
        """
        # bounds = ((1, 3), (4, 10), (8, 15))
        # lb = 10
        # ub = 15
        # example.add_linear_constraint(bounds, lb, ub)
        terms = []

        for bound in bounds:
            lower_bound = self.model.NewIntVar(0, bound[0], 'x')
            upper_bound = bound[1]

            terms.append([lower_bound, upper_bound])

        self.model.AddLinearConstraint(terms, 10, 15)

    def add_sum_constraint(self, variables, lower_bound, upper_bound):
        """
        Args:
            lower_bound: int
            upper_bound: int
            variables: list/tuple of ints

        example call:
        class_name.add_sum_constraint((2, 4, 5, 6), 2, 5)
        """

        # convert the list of integers to a list with Integer Variables objects
        variables = [
            self.model.NewIntVar(0, variable, str(variable))
            for variable in variables
            ]

        self.model.AddSumConstraint(variables, lower_bound, upper_bound)

    def add_linear_constraint_with_bounds(
            self,
            terms: List[int],
            bounds: List[int]):
        """
        Args:
            terms: List[int]/Tuple[int]
            bounds: List[int]/Tuple[int]
        Example call:
            example.add_linear_constraint_with_bounds((7, 5, 6), (1, 2, 3, 4))
        """

        # Bounds must be converted to IntVar variables.
        _bounds = [
            (self.create_integer_variable(bound, bound, str(bound)), 0)
            for bound in bounds
            ]

        self.model.AddLinearConstraintWithBounds(_bounds, terms)

    def add_all_different_constraint(self, variables: List[int]):
        """
        Args:
            variables: list or tuple that contains integers
            EXAMPLE.add_all_different_constraint((7, 5, 6))
        """
        _variables = [
            self.create_integer_variable(variable, variable, str(variable))
            for variable in variables]
        print(_variables)
        self.model.AddAllDifferent(_variables)

    def add_element(self, index: int, variables: List[int], target: int):
        """
        Args:
            variables: list or tuple that contains integers
            index: integer
            target: integer
        Example call:
            EXAMPLE.add_element(0, (1, 2, 3), 5)
        """

        # convert both target and each variable in variables list
        # to an IntVar variable.
        _target = self.create_integer_variable(target, target, str(target))
        _variables = [
            self.create_integer_variable(variable, variable, str(variable))
            for variable in variables]

        self.model.AddElement(index, _variables, _target)

    def add_allowed_assignments(
            self,
            variables: List[int],
            tuples_list: List[Tuple[int]]):
        """
        Args:
            variables: list or tuple that contains integers
            tuples_list: list of tuples that contain integers
        Example call:
            EXAMPLE.add_allowed_assignments([1, 2, 3], [(9, 3, 5), (5, 6, 3)])
        """
        self.model.AddAllowedAssignments(variables, tuples_list)

    def add_forbidden_assignments(
            self,
            variables: List[int],
            forbidden_assignments_list: List[Tuple[int]]):
        """
        Args:
            variables: A list of variables.
            tuples_list: A list of forbidden tuples. Each tuple must have the same
            length as the variables, and the ith value of a tuple corresponds to the
            ith variable.

        Example call:
            EXAMPLE.add_forbidden_assignments([1, 2, 3], [(2, 3, 4), (1, 2, 5)])
        """
        self.model.AddForbiddenAssignments(variables, forbidden_assignments_list)

    def add_inverse(self, variables: List[int], inverse_variables: List[int]):
        """
        example call:
            EXAMPLE.add_inverse([1, 2], [2, 1])
        """
        self.model.AddInverse(variables, inverse_variables)


if __name__ == "__main__":
    EXAMPLE = ConstraintExample()
    tmp = cp_model.CpModel()
    x = [1, 2, 3, 2]
    solver = cp_model.CpSolver()

    _x = [tmp.NewIntVar(item, item, str(item)) for item in x]

    tmp.AddAllDifferent(_x)

    # EXAMPLE.add_all_different_constraint(x)
    status = solver.Solve(tmp)
    print(status)

    # if status == cp_model.FEASIBLE:
        # for item in _x:
            # print(solver.Value(item))

