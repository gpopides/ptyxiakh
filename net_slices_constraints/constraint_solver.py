from ortools.sat.python import cp_model

class NetworkConstraintSolver:
    def __init__(self, annotated_links, slice_graph):
        self.annotated_links = annotated_links
        self.slice_graph = slice_graph
        self.model = cp_model.CpModel()
        self.solver = cp_model.CpSolver()

        self.links_dict = {}
        self.dc_parts_dict = {}

        self.cost_per_link = {}

    def set_up_links_constraints(self):
        net_links = [x['name'] for x in self.slice_graph['net_parts']]
        links_dict = {}

        for link in net_links:
            links_dict[link] = {
                "providers": [],
                "links": [],
                "costs_as_ints": [],  # this will be used to display the dc parts in result,
                "costs_selected": []

            }

        for index, annotated in enumerate(self.annotated_links):
            link_name = f"{annotated['net-part-id']}_{annotated['provider']}_{index}"

            _cost_as_int = int(annotated['total_cost'] * 10000)
            # cost_as_int = self.model.NewIntVarFromDomain(cp_model.Domain.FromValues([0, _cost_as_int]), link_name)

            links_dict[annotated['net-part-id']]["costs_as_ints"].append(_cost_as_int)
            links_dict[annotated['net-part-id']]["costs_selected"].append(self.model.NewBoolVar(link_name))
            links_dict[annotated['net-part-id']]["providers"].append(link_name)
            links_dict[annotated['net-part-id']]["links"].append(annotated['links'])

        for item in links_dict.values():
            # each link has a min and max cost which are the bounds
            # for the selected cost
            item['upper_bound'] = max(item['costs_as_ints'])
            item['lower_bound'] = min(item['costs_as_ints'])

        self.links_dict = links_dict

    def create_dc_parts_collection(self):
        for annotated in self.annotated_links:
            for dc_part in annotated['links'].keys():
                if dc_part not in self.dc_parts_dict:
                    dc_part_providers = self._get_dc_part_providers_as_domain(dc_part)
                    dc_part_int_var = self.model.NewIntVarFromDomain(
                        cp_model.Domain.FromValues(dc_part_providers),
                        dc_part
                    )
                    self.dc_parts_dict[dc_part] = dc_part_int_var

    def _get_dc_part_providers_as_domain(self, dc_part):
        # create an list of  numbers that represent which providers
        # offer the dc part
        # e.g if dcProv1 and dcProv5 offer the specific dc part
        # the result will be [1, 5]
        providers = []

        for annotated in self.annotated_links:
            if dc_part in annotated['links'].keys() and annotated['links'][dc_part] not in providers:
                providers.append(annotated['links'][dc_part])

        provider_symbolic_numbers = [int(provider.replace('dcProv', '')) for provider in providers]

        return provider_symbolic_numbers

    def set_constraints(self):
        costs = []  # all the costs for each link between all nodes
        indexes = []

        for link, link_data in self.links_dict.items():
            best_cost_of_link = self.model.NewIntVar(link_data['lower_bound'], link_data['upper_bound'], link)
            index = self.model.NewIntVar(0, len(link_data['costs_as_ints']), f'index_{link}')

            for i, cost in enumerate(link_data['costs_as_ints']):
                # this is used to indicate which cost from the list was selected
                # so in the next step, we can force the dc provider per dc part constraint
                self.model.Add(index == i).OnlyEnforceIf(link_data['costs_selected'][i])
                # this constraints is used so the solver ignores the cost at the specific index
                # when will be searching for a solution
                self.model.Add(index != i).OnlyEnforceIf(link_data['costs_selected'][i].Not())

            self.model.AddElement(index, link_data['costs_as_ints'], best_cost_of_link)

            costs.append(best_cost_of_link)
            indexes.append(index)

        # in this step, we loop again over the net links and
        # set the dc provider per dc part constraint
        # if value of the bool var at the specific index is true
        # we must set the constraint that the value of the dc part
        # is the same with the dc part of the current link
        for _link, _link_data in self.links_dict.items():
            for i, link in enumerate(_link_data['links']):
                for dc_part, dc_prov in link.items():
                    # get the number from the string dcProv1 -> 1
                    prov_num = int(dc_prov.replace("dcProv", ""))
                    # if the cost selected is true, the value of the DcPart IntVar must have the value of the dc part in
                    # the link
                    self.model.Add(self.dc_parts_dict[dc_part] == prov_num).OnlyEnforceIf(_link_data['costs_selected'][i])

        return costs, indexes

    def display_solution_result(self, sum_cost, cost_indexes):
        status = self.solver.Solve(self.model)
        print(self.solver.StatusName(status))

        # for cost_index in cost_indexes:
        #     index_value = self.solver.Value(cost)


        if status in [cp_model.FEASIBLE, cp_model.OPTIMAL]:
            total_cost = self.solver.Value(sum_cost) / 10000
            print(f"Total cost {total_cost}")

            for i, link in enumerate(self.links_dict):
                cost_index = self.solver.Value(cost_indexes[i])
                selected_net_provider = self.links_dict[link]['providers'][cost_index]

                print(f"Net provider: {selected_net_provider}")

            for dc_part, dc_part_prov in self.dc_parts_dict.items():
                print(f"{dc_part}: Provider {self.solver.Value(dc_part_prov)}")

    def solve(self):
        self.create_dc_parts_collection()
        self.set_up_links_constraints()

        costs, cost_indexes = self.set_constraints()
        sum_cost = sum(costs)
        self.model.Minimize(sum_cost)

        self.display_solution_result(sum_cost, cost_indexes)


