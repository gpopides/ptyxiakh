import argparse
import sys
import json
from pathlib import Path
import importlib
from typing import List

PROBLEMS_DICT = {}


def register_methods(modules_collection, module_names: List[str]):
    """
    For each imported module, register to the PROBLEMS_DICT
    the module name and it's "solver" method.
    """
    for module, module_name in zip(modules_collection, module_names):
        try:
            solver_method = getattr(module, f"solve_{module_name}")
            PROBLEMS_DICT[module_name] = solver_method
        except AttributeError:
            sys.stderr.write(f"solve_{module_name} does not exist in {module_name}")


def import_solved_example_modules(directory_path):
    """
    Import the python files inside the solved_samples directory.
    """
    file_names = [
        _file.stem
        for _file in directory_path.glob("*.py")
        if _file.name not in ("__init__.py", "general.py")
    ]
    imported_modules = []

    for file_name in file_names:
        imported_modules.append(importlib.import_module(f"solved_samples.{file_name}"))

    register_methods(imported_modules, file_names)


def parser_factory():
    parser = argparse.ArgumentParser(
        description="Runs constraint programming solving problems"
    )
    parser.add_argument(
        "-ap",
        "--available_problems",
        help="display available problems",
        action="store_true",
    )
    parser.add_argument("-p", "--problem", help="example to run")
    parser.add_argument("-df", "--data_file", help="file with data")

    return parser.parse_args()


def register_problem_methods():
    problem_files_directory = Path.cwd().joinpath("solved_samples")
    if not Path.exists(problem_files_directory):
        raise Exception("directory with the solved examples does not exist")

    import_solved_example_modules(problem_files_directory)


def show_prompt():
    parser = parser_factory()

    if parser.available_problems:
        print("Avaliable problems: \n")
        for problem in PROBLEMS_DICT:
            print(problem)
        sys.exit()
    elif not parser.data_file or not parser.problem:
        sys.stderr.write("arguments not provided, see --help")
        sys.exit()

    if PROBLEMS_DICT.get(parser.problem):
        try:
            with open(parser.data_file, "r") as data_file:
                problem_data = json.load(data_file)
            PROBLEMS_DICT[parser.problem](problem_data)
        except FileNotFoundError:
            sys.stderr.write(f"File {parser.data_file} does not exist")
            sys.exit()


if __name__ == "__main__":
    register_problem_methods()
    show_prompt()
    # print(PROBLEMS_DICT)
