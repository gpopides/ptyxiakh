from ortools.sat.python import cp_model

CRED = '\033[91m'
CEND = '\033[0m'


def solve_lights():
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    light_weights = [20, 30, 50, 60, 90, 100, 150, 250, 500]
    light_placements = [-5, 5, 20, 40]

    axis_index = 0

    max_bound = max(light_weights)
    min_bound = min(light_weights)

    indexes_left_of_axis, indexes_right_of_axis = light_placements[:1], light_placements[1:]

    left_vars, right_vars = [], []
    left_distances, right_distances = [], []

    for index in indexes_left_of_axis:
        distance = abs(index - axis_index)
        allowed_assignments = [[weight * distance] for weight in light_weights]

        _var = my_model.NewIntVar(min_bound * distance, max_bound * distance, str(index))

        left_vars.append(_var)
        left_distances.append(distance)

        my_model.AddAllowedAssignments([_var], allowed_assignments)

    for index in indexes_right_of_axis:
        distance = abs(index - axis_index)
        allowed_assignments = [[weight * distance] for weight in light_weights]

        _var = my_model.NewIntVar(min_bound * distance, max_bound * distance, str(index))

        right_vars.append(_var)
        right_distances.append(distance)

        my_model.AddAllowedAssignments([_var], allowed_assignments)

    my_model.Add(sum(right_vars) == sum(left_vars))

    my_model.AddAllDifferent([*left_vars, *right_vars])

    status = solver.Solve(my_model)
    print(solver.StatusName(status))

    if solver.StatusName(status) == "FEASIBLE":
        print(f"{CRED}LEFT SIDE OF AXIS{CEND}")

        print(f"Weight Sum: {sum([solver.Value(var) for var in left_vars])}")

        for l_var, distance in zip(left_vars, left_distances):
            value = solver.Value(l_var)
            light_weight = value//distance
            print(f"Light Weight: {light_weight} (Index {light_weights.index(light_weight)} in list)")

        print(f"{CRED}RIGHT SIDE OF AXIS{CEND}")

        print(f"Sum: {sum([solver.Value(var) for var in right_vars])}")

        for r_var, distance in zip(right_vars, right_distances):
            value = solver.Value(r_var)
            light_weight = value//distance
            print(f"Light Weight: {light_weight} (Index {light_weights.index(light_weight)} in list)")


if __name__ == '__main__':
    solve_lights()
