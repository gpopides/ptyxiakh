from ortools.sat.python import cp_model


def solve_car(schedule):
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()
    jobs = []
    car_variables = []

    # for each car we have duration of it's electrical and mechanical needs
    # so we will create as many IntervalVars as many cars we have
    for car_name, items in schedule.items():
        print(car_name)
        print(items)

        # total duration for each car is the sum of the mechanical
        # and electrical needs
        duration = items["electrical"] + items["mechanical"]

        start_time = model.NewIntVar(0, 1000, f"start_{car_name}")
        end_time = model.NewIntVar(0, 1000, f"end_{car_name}")

        model.Add(end_time == start_time + duration)

        # the car service must start after the release
        model.Add(start_time >= items["release"])

        jobs.append(
            model.NewIntervalVar(start_time, duration, end_time, car_name)
        )
        # this will be used for showing the results
        car_variables.append({"car": car_name, "start": start_time, "end": end_time})

    model.AddNoOverlap(jobs)

    makespan = model.NewIntVar(0, 1000, 'makespan')
    # makespan is the end of the project
    model.AddMaxEquality(makespan, [v["end"] for v in car_variables])
    # minimize the sum of ends for each task
    model.Minimize(makespan)
    status = solver.Solve(model)

    print(solver.StatusName(status))

    for v in car_variables:
        print(v["car"], solver.Value(v["start"]),solver.Value(v["end"]))
