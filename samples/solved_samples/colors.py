from ortools.sat.python import cp_model


"""
This is a solution with GOOGLE OR TOOLS for the problem provided by IBM.

link: https://ibmdecisionoptimization.github.io/docplex-doc/cp/basic.color.py.html

The problem involves choosing colors for the countries on a map in
such a way that at most four colors (blue, white, yellow, green) are
used and no neighboring countries are the same color. In this exercise,
you will find a solution for a map coloring problem with six countries:
"France", "Germany", "Denmark", "Spain", "Portugal"
"""


def solve_colors():
    colors = ("Yellow", "Red", "Green", "Blue")

    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    # create the variables
    country_vars = [
        my_model.NewIntVar(0, len(colors), ctr) for ctr in [
            "France", "Germany", "Denmark", "Spain", "Portugal"
        ]
    ]

    neighbours_dict = {}

    for c_var in country_vars:
        # Germany has Denmark and France as neighbours
        if c_var.Name() == "Germany":
            neighbours = [n for n in country_vars if n.Name() in ['Denmark', 'France']]
            neighbours_dict[c_var.Name()] = {
                "country_var": c_var,
                "neighbours": neighbours
            }
        # spain has portugal and france
        elif c_var.Name() == "Spain":
            neighbours = [n for n in country_vars if n.Name() in ['Portugal', 'France']]
            neighbours_dict[c_var.Name()] = {
                "country_var": c_var,
                "neighbours": neighbours
            }

    for vars_dict in neighbours_dict.values():
        _country = vars_dict["country_var"]
        _neighbours = vars_dict["neighbours"]
        # for each country in the list, enforce the constraint
        # that the country does not have the same color with it's
        # neighbours
        for neighbour in _neighbours:
            my_model.Add(neighbour != _country)

    status = solver.Solve(my_model)

    if solver.StatusName(status) == "FEASIBLE":
        for cv in country_vars:
            print(f"{cv}: {colors[solver.Value(cv)]}")


if __name__ == '__main__':
    solve_colors()

