from ortools.sat.python import cp_model


def linear_sample():
    """
        Example to solve 10 < 3x+2y+z < 15
    """
    # declare the model and the solver
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    x = model.NewIntVar(0, 10, 'x')
    y = model.NewIntVar(0, 10, 'y')
    z = model.NewIntVar(0, 10, 'z')

    # declare the bounds as native python int.
    lower_bound = 10
    upper_bound = 15

    linear_expression = 3 * x + 2 * y + z

    model.AddLinearConstraint(linear_expression, lower_bound, upper_bound)

    status = solver.Solve(model)
    print(solver.StatusName(status))

    if status == cp_model.FEASIBLE:
        for variable in [x, y, z]:
            print(f"Variable {variable}: {solver.Value(variable)}")


def sum_constraint():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    variables = [
        model.NewIntVar(0, 5, var)
        for var in ('x', 'y')
    ]

    model.Add(sum(variables) > 1)

    status = solver.Solve(model)
    print(solver.StatusName(status), solver.Value(variables[0]), solver.Value(variables[1]))


def all_different_constraint_sample():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()
    variables_list = ['x', 'y', 'z', 'w']
    variables = [
        model.NewIntVar(0, len(variables_list), var)
        for var in variables_list
    ]

    model.AddAllDifferent(variables)
    status = solver.Solve(model)


def add_element_constraint():
    """
    append variable z on the variables constraints list
    """
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()
    z = model.NewIntVar(0, 10, "z")

    # create the variables x,y
    variables = [
        model.NewIntVar(0, 10, var)
        for var in ["x", "y"]
    ]

    model.AddElement(len(variables) - 1, variables, z)
    status = solver.Solve(model)
    print(f"Solution: {solver.StatusName(status)}")


def circuit_constraint_sample():
    model = cp_model.CpModel()

    # node 0 goes to node 1
    arcs = [(0, destination, model.NewBoolVar("arc0->arc{destination}")) for destination in [1]]
    # node 1 goes to node 2
    arcs.extend([(1, destination, model.NewBoolVar("arc1->arc{destination}")) for destination in [2]])
    # node 2 can go to both nodes 0, 1
    arcs.extend([(2, destination, model.NewBoolVar(f"arc2->arc{destination}")) for destination in [0, 1]])

    model.AddCircuit(arcs)
    solver = cp_model.CpSolver()
    status = solver.Solve(model)

    print(solver.StatusName(status))

    print('One means that the arc is in the Hamiltonian Path of the graph')
    print('Zero means it is not.')
    for a in arcs:
        print('arc: from: %(from)i to: %(to)i status: %(2)i' % {'from': a[0], 'to': a[1],
                                                                '2': solver.Value(a[2])})


def allowed_assignements_sample():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    vars_list = ['x', 'y']
    variables = [
        model.NewIntVar(0, 20, var)
        for var in vars_list
    ]

    # x can be either 2 or 3
    # y can be either 5 or 7
    assignments_tuple = [(2, 5), (3, 7)]

    model.AddAllowedAssignments(variables, assignments_tuple)
    status = solver.Solve(model)
    print(f"Solution: {solver.StatusName(status)}")
    print(f"Value x: {solver.Value(variables[0])}")
    print(f"Value y: {solver.Value(variables[1])}")


def forbidden_assignments_sample():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    variables = [
        model.NewIntVar(0, 20, var)
        for var in ['x', 'y']
    ]

    # x can not be either 2 or 3
    # y can not be either 5 or 7
    assignments_tuple = [(2, 5), (3, 7)]

    model.AddForbiddenAssignments(variables, assignments_tuple)
    status = solver.Solve(model)
    print(f"Solution: {solver.StatusName(status)}")


def automata_sample():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    transition_variables = [
        model.NewIntVar(0, 1, var)
        for var in ('q5', 'q6', 'q7')
    ]
    starting_state = 5

    final_states = [7]

    transitions = [
        (5, 1, 6),  # q5 with input 1 goes to q6
        (5, 0, 5),  # q5 with input 0 stays at q5
        (6, 1, 6),  # q6 with input 1 stays at q6
        (6, 0, 7),  # q6 with input 0 goes to q7
        (7, 1, 6),  # q7 with input 1 goes to q7
        (7, 0, 6),  # q7 with input 0 goes to q6
    ]

    model.AddAutomaton(transition_variables, starting_state, final_states, transitions)

    status = solver.Solve(model)
    print(f"Solution: {solver.StatusName(status)}")


def inverse_sample():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    variables = [model.NewIntVar(0, 1, var) for var in ('x', 'y')]
    inverse_variables = [model.NewIntVar(0, 2, var) for var in ('y', 'x')]

    model.AddInverse(variables, inverse_variables)

    status = solver.Solve(model)
    print(f"Solution: {solver.StatusName(status)} \n")

    for variable, inverse_variable in zip(variables, inverse_variables):
        print(f"{solver.Value(variable)} {solver.Value(inverse_variable)}")


def reservoir_sample():
    max_reservoir_level = 100
    min_reservoir_level = 1
    times = [0, 10, 23, 40, 48, 60]
    demands = [20, 30, 30, -70, 0, 0]

    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    model.AddReservoirConstraint(times, demands, min_reservoir_level, max_reservoir_level)

    status = solver.Solve(model)
    print(f"Solution: {solver.StatusName(status)} \n")


def bool_or():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()
    # 0 = true, 1 = false

    x = model.NewBoolVar('x')
    y = model.NewBoolVar('y')

    model.AddBoolOr([x, y])

    status = solver.Solve(model)
    print(solver.StatusName(status), solver.Value(x), solver.Value(y))


def bool_and():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    x = model.NewBoolVar('x')
    y = model.NewBoolVar('y')

    model.AddBoolAnd([x, y])

    status = solver.Solve(model)
    print(solver.StatusName(status), solver.Value(x), solver.Value(y))


def bool_xor():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    x = model.NewBoolVar('x')
    y = model.NewBoolVar('y')

    model.AddBoolXOr([x, y])

    status = solver.Solve(model)
    print(solver.StatusName(status), solver.Value(x), solver.Value(y))


def max_equality():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    x = model.NewIntVar(2, 10, 'x')
    y = model.NewIntVar(2, 5, 'y')
    z = model.NewIntVar(1, 1, 'z')

    model.AddMaxEquality(z, [x, y])

    status = solver.Solve(model)
    print(solver.StatusName(status))

    if solver.StatusName(status) == "FEASIBLE":
        for var in [x, y]:
            print(solver.Value(var))


def min_equality():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    x = model.NewIntVar(1, 10, 'x')
    y = model.NewIntVar(1, 5, 'y')
    z = model.NewIntVar(0, 1, 'z')

    model.AddMinEquality(z, [x, y])

    status = solver.Solve(model)
    print(solver.StatusName(status))


def no_overlap():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    # create the jobs
    job_a = model.NewIntervalVar(0, 20, 20, 'job_a')
    job_b = model.NewIntervalVar(19, 5, 24, 'job_b')

    # add the constraint that for job_b to wait job_a
    model.AddNoOverlap([job_a, job_b])

    status = solver.Solve(model)
    print(solver.StatusName(status))


def no_overlap_2d():
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    intervals_dict = {
        'a': {
            'start': [0, 5],
            'size': [5, 5],
            'name': ['x', 'y']
        },
        'b': {
            'start': [12, 14],
            'size': [3, 20],
            'name': ['z', 'w']
        }
    }

    intervals_x, intervals_y = [], []

    for i in range(2):
        start_a = intervals_dict['a']['start'][i]
        size_a = intervals_dict['a']['size'][i]
        end_a = start_a + size_a

        start_b = intervals_dict['b']['start'][i]
        size_b = intervals_dict['b']['size'][i]
        end_b = start_b + size_b

        intervals_x.append(model.NewIntervalVar(
            start_a,
            size_a,
            end_a,
            intervals_dict['a']['name'][i],
        ))
        intervals_y.append(model.NewIntervalVar(
            start_b,
            size_b,
            end_b,
            intervals_dict['b']['name'][i],
        ))

    model.AddNoOverlap2D(intervals_x, intervals_y)

    status = solver.Solve(model)
    print(solver.StatusName(status))


def cumulative_sample():
    demands = [20, 30]
    capacity = 100

    model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    job_a = model.NewIntervalVar(0, 20, 20, 'job_a')
    job_b = model.NewIntervalVar(20, 5, 25, 'job_b')

    model.AddCumulative([job_a, job_b], demands, capacity)

    status = solver.Solve(model)
    print(solver.StatusName(status))


if __name__ == "__main__":
    no_overlap()

