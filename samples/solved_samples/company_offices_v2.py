from ortools.sat.python import cp_model
import argparse
import sys
import json

MAX_OFFICES_PER_FLOOR = 40
MAX_FLOORS = 2



# Clearing Up things and introducing comments.
def solve_company_offices_v2(problem_data):
    #model creation
    office_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    section_interval_variables = []
    offices_needs_per_section = []

    start_vars = {}
    end_vars = {}

    # we have 3 pairs that should be on the same floor
#    pair_1_start = my_model.NewIntVar(0, 3, 'pair_1_start')
#    pair_2_start = my_model.NewIntVar(0, 3, 'pair_2_start')
#    pair_3_start = my_model.NewIntVar(0, 3, 'pair_3_start')

    offices_variables = []

    max_offices = office_model.NewIntVar(0, 1000, 'max')

    # the pairs, must have the same start(which means same floor)
    for section, offices_count in problem_data.items():
        # t1 t3 is the first pair
        # Creating Start and end Variables for the cumulative constraint.
        start = office_model.NewIntVar(0,MAX_FLOORS,f"{section}_start")
        end = office_model.NewIntVar(0,MAX_FLOORS + 1,f"{section}_end")
        # Creating an interval var for the cumulative constraint
        # and adding this to the list.
        section_interval_variables.append(
                office_model.NewIntervalVar(start, 1, end, section)
        )
        # Demands for offices by each section
        offices_needs_per_section.append(offices_count)
        # keep refs to start, end vars
        start_vars[section] = start
        end_vars[section] = end

    office_model.AddCumulative(
        section_interval_variables,
        offices_needs_per_section,
        MAX_OFFICES_PER_FLOOR
    )

    # Specify which offices must be on the same floor.
    # Adding equality constraints (easily extendable)
    office_model.Add(start_vars['t1'] == start_vars['t3'])
    office_model.Add(start_vars['t2'] == start_vars['t4'])
    office_model.Add(start_vars['t8'] == start_vars['t9'])

    #some tests on vars and information
    # this is before Solve to have a pick at was goes under the hood.
    for var in start_vars.values():
        print(var, var.Index(), var.Proto(), var.GetVarValueMap())

    # Let us solve this.
    status = solver.Solve(office_model)

    print(solver.StatusName(status))
    solution = {}
    # This is not yet an optimization problem
    # So we get one solution
    if status == cp_model.FEASIBLE:
        for floor in list(range(0,MAX_FLOORS + 1)):
            solution[f"floor_{floor}"] = [
                section for section, var in start_vars.items()
                if solver.Value(var) == floor
                ]
    # and now print them
    for floor in list(range(0,MAX_FLOORS + 1)):
        print(solution[f"floor_{floor}"])

if __name__ == '__main__':
    try:
        with open(sys.argv[1], 'r') as data_file:
            problem_data = json.load(data_file)
        print(" Solving the Offices Problem")
        solve_company_offices_v2(problem_data)
    except FileNotFoundError:
        sys.stderr.write(f"File {sys.argv[1]} does not exist")
        sys.exit()
