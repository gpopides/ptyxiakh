from ortools.sat.python import cp_model


def solve_puzzle():
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    square = my_model.NewIntVar(0, 30, 'square')
    square_2 = my_model.NewIntVar(0, 30, 'square_2')
    circle = my_model.NewIntVar(0, 30, 'circle')

    my_model.Add((3 * square) == 30)
    my_model.Add(square - circle == 2)
    my_model.Add((2 * square_2) + square == 18)

    status = solver.Solve(my_model)

    print(solver.StatusName(status))

    total_sum = sum([solver.Value(var) for var in [square_2, square, circle]])

    print("SQUARE + SQUARE_2 + CIRCLE = {}".format(total_sum))


if __name__ == '__main__':
    solve_puzzle()
