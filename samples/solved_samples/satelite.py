"""
Problem 7
"""
from ortools.sat.python import cp_model

MAX_WATTS = 18


def solve_satelite():
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    task_vars = []
    tasks = ['photo_shoot', 'edit', 'send', 'fix']
    task_durations = (5, 6, 8, 4)
    task_watt_demands = (5, 12, 8, 10)
    task_start_and_end_dict = {}


    # create the IntervalVar for each task
    for task, duration in zip(tasks, task_durations):
        start = my_model.NewIntVar(0, 25, f"{task}_start")
        end = my_model.NewIntVar(0, 25, f"{task}_end")

        task_start_and_end_dict[task] = {
            "start": start,
            "end": end
        }

        task_vars.append(my_model.NewIntervalVar(start, duration, end, task))


    # add the constraints 
    for task, items in task_start_and_end_dict.items():
        # photo shoot must end before Time 10.
        if task == "photo_shoot":
            my_model.Add(items["end"] < 10)
        elif task == "edit":
            # edit bust start after the photo shoot start + 2 Time units
            my_model.Add(items["start"] >= task_start_and_end_dict["photo_shoot"]["start"] + 2)
        elif task == "send":
            # send must start after the end of edit task
            my_model.Add(items["start"] > task_start_and_end_dict["edit"]["end"])
        else:
            # fix must end before the last time unit
            my_model.Add(items["end"] < 25)

    # add the cumulative for each task
    my_model.AddCumulative(task_vars, task_watt_demands, MAX_WATTS)

    status = solver.Solve(my_model)

    print(solver.StatusName(status))

    if status == cp_model.OPTIMAL:
        for task, items in task_start_and_end_dict.items():
            print("Task {:<4} Start {:<4} End {:<4} ".format(
                task.upper(),
                solver.Value(items["start"]),
                solver.Value(items["end"]),
            ), end="\n")


if __name__ == '__main__':
    solve_satelite()
