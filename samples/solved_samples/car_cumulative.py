from ortools.sat.python import cp_model
import sys
import json
from functools import reduce

def solve_car_cumulative(schedule):
    model = cp_model.CpModel()
    solver = cp_model.CpSolver()
    jobsE = []
    jobsM = []
    car_variables = []

    MAX_DURATION = reduce(lambda x,y: x+y ,[demand[key] for key in ['electrical','mechanical','release'] for car,demand in schedule.items()])
    print("Max Possible Duration:",MAX_DURATION)


    # for each car we have duration of it's electrical and mechanical needs
    # so we will create as many IntervalVars as many cars we have
    for car_name, car_demands in schedule.items():
        print(car_name, "::", car_demands)

        # Constraints regarding the Electrical jobs neede for the car
        start_time_e = model.NewIntVar(0, MAX_DURATION, f"startE_{car_name}")
        end_time_e = model.NewIntVar(0, MAX_DURATION, f"endE_{car_name}")
        model.Add(end_time_e == start_time_e + car_demands['electrical'])
        # the car service must start after the release
        model.Add(start_time_e >= car_demands["release"])
        interval_varE = model.NewIntervalVar(start_time_e, car_demands['electrical'], end_time_e, f"E{car_name}")

        ## Adding this to the list of Electrical jobs
        jobsE.append(interval_varE)

        # Constraints regarding the Mechanical jobs neede for the car
        start_time_m = model.NewIntVar(0, MAX_DURATION, f"startM_{car_name}")
        end_time_m = model.NewIntVar(0, MAX_DURATION, f"endM_{car_name}")
        model.Add(end_time_m == start_time_m + car_demands['mechanical'])
        # the car service must start after the release
        model.Add(start_time_m >= car_demands["release"])
        interval_varM = model.NewIntervalVar(start_time_m, car_demands['mechanical'], end_time_m, f"M{car_name}")

        ## Adding this to the list of Mechanical Jobs
        jobsM.append(interval_varM)

        # Cannot do both on the same car
        model.AddNoOverlap([interval_varE,interval_varM])
        #REMOVE
        #jobs.append(
        #    model.NewIntervalVar(start_time, duration, end_time, car_name)
        #)
        # this will be used for showing the results
        car_variables.append({"car": car_name, "startE": start_time_e, "endE": end_time_e, "startM": start_time_m, "endM": end_time_m, })

    # Cumulative

    # We assume that each job needs one worker and there are 1 electrician and one car mechanic
    model.AddCumulative(jobsE,[1 for n in jobsE],1)
    model.AddCumulative(jobsM,[1 for n in jobsM],1)

    makespan = model.NewIntVar(0, MAX_DURATION, 'makespan')
    ends = [var["endE"] for var in car_variables]
    ends.extend([var["endM"] for var in car_variables])
    # makespan is the end of the project
    model.AddMaxEquality(makespan, ends)
    # minimize the sum of ends for each task
    model.Minimize(makespan)
    status = solver.Solve(model)

    print(solver.StatusName(status))

    print("Solution")
    for v in car_variables:
        print(v["car"], "Electrical:", solver.Value(v["startE"]),"Mechanical:", solver.Value(v["startM"]))

    print("makespan:",solver.Value(makespan))

