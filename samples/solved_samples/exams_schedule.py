"""
Solution for problem 9
"""

from ortools.sat.python import cp_model


def solve_exams_schedule(problem_data=None):
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    course_intervals = []
    course_start_end_dates_list = []

    study_makespans = []

    for course, items in problem_data.items():
        start = my_model.NewIntVar(1, 30, f'{course}_start')
        end = my_model.NewIntVar(1, 30, f'{course}_end')

        # create the study interval variable
        course_intervals.append(
            my_model.NewIntervalVar(start, items['duration'], end, course)
        )

        # this will be used to show the results
        course_start_end_dates_list.append((start, end))

        # the study end must be before the date of exams
        my_model.Add(end < items["exams_date"])

        study_makespans.append(items['duration'])

    my_model.AddNoOverlap(course_intervals)

    # we need the smallest study durations
    my_model.Minimize(sum(study_makespans))

    status = solver.Solve(my_model)

    print(solver.StatusName(status))
    print("*" * 20)

    if status == cp_model.OPTIMAL:
        for course, start_end_tuple in zip(problem_data, course_start_end_dates_list):
            start, end = start_end_tuple
            print(f"{course.upper()}: START DATE {solver.Value(start)} END DATE {solver.Value(end)}")


if __name__ == "__main__":
    solve_exams_schedule()
