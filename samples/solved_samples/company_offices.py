from ortools.sat.python import cp_model

MAX_OFFICES_PER_FLOOR = 40


def solve_company_offices(problem_data):
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    section_interval_variables = []
    offices_need_per_section = []

    start_and_ends_dict = {}

    # we have 3 pairs that should be on the same floor
    pair_1_start = my_model.NewIntVar(0, 3, 'pair_1_start')
    pair_2_start = my_model.NewIntVar(0, 3, 'pair_2_start')
    pair_3_start = my_model.NewIntVar(0, 3, 'pair_3_start')

    offices_variables = []

    max_offices = my_model.NewIntVar(0, 1000, 'max')

    # the pairs, must have the same start(which means same floor)
    for section, offices_count in problem_data.items():
        # t1 t3 is the first pair
        if section in ('t1', 't3'):
            start = pair_1_start
        # t2 t4 is the second pair
        elif section in ('t2', 't4'):
            start = pair_2_start
        # and t8 t9 the last
        elif section in ('t8', 't9'):
            start = pair_3_start
        else:
            # if the section is not in a pair, it can be in whatever floor
            start = my_model.NewIntVar(0, 3, f"{section}_start")

        end = my_model.NewIntVar(0, 3, f"{section}_end")

        section_interval_variables.append(
                my_model.NewIntervalVar(start, 1, end, section)
        )

        offices_need_per_section.append(offices_count)
        offices_variables.append(
                my_model.NewIntVar(0, 40, f"{section}_offices")
        )

        start_and_ends_dict[section] = {'start': start, 'end': end}

    my_model.AddMaxEquality(
        max_offices, [office_count for office_count in offices_variables]
    )

    my_model.AddCumulative(
        section_interval_variables,
        offices_need_per_section,
        MAX_OFFICES_PER_FLOOR
    )

    my_model.Minimize(max_offices)

    status = solver.Solve(my_model)

    print(solver.StatusName(status))

    if status == cp_model.OPTIMAL:
        floor_1 = [
            section for section, pair in start_and_ends_dict.items()
            if solver.Value(pair['start']) == 0
        ]
        floor_2 = [
            section for section, pair in start_and_ends_dict.items()
            if solver.Value(pair['start']) == 1
        ]
        floor_3 = [
            section for section, pair in start_and_ends_dict.items()
            if solver.Value(pair['start']) == 2
        ]

        print(f"Floor 1 {floor_1} \nFloor 2 {floor_2} \nFloor 3 {floor_3}")
