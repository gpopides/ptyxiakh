from ortools.sat.python import cp_model


def solve_cloud(providers_data):
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    # these are the variables for the GB storage of each provider
    storage_variables = [
        my_model.NewIntVar(0, 2000, f'{provider}') 
        for provider in providers_data
    ]
    # and these are the variables for their prices
    price_variables = [
        my_model.NewIntVar(0, 22, f'{provider}_price') 
        for provider in providers_data
    ]

    # use these to show the results
    gbs_tuples_list = []
    prices_tuples_list = []

    for provider, items in providers_data.items():
        gbs_tuples_list.append(items['gb'])
        prices_tuples_list.append(items['price'])

    for gbs_tuple, prices_tuple, variable, price_var in zip(
            gbs_tuples_list,
            prices_tuples_list,
            storage_variables,
            price_variables
    ):
        # for each provider we can pick only one gb plan of the list
        # which means that we need to create a list with the allowed storage
        # plans
        allowed_gbs = [(allowed,) for allowed in gbs_tuple]
        # same for the price
        allowed_prices = [(allowed,) for allowed in prices_tuple]

        # add the constraint
        my_model.AddAllowedAssignments([price_var], allowed_prices)
        my_model.AddAllowedAssignments([variable], allowed_gbs)

    # storage plans must not exceed 4600GBS and must be less than 3600
    my_model.Add(sum(storage_variables) > 3600)
    my_model.Add(sum(storage_variables) < 4600)

    # we need the best combination so we want to pay the least.
    my_model.Minimize(sum(price_variables))

    status = solver.Solve(my_model)

    print(solver.StatusName(status))

    if status in [cp_model.FEASIBLE, cp_model.OPTIMAL]:
        sum_gbs = 0
        sum_price = 0
        for gb_var in storage_variables:
            gb_var_value = solver.Value(gb_var)
            gb_index = providers_data[gb_var.Name()]['gb'].index(gb_var_value)
            price = providers_data[gb_var.Name()]['price'][gb_index]

            sum_gbs += gb_var_value
            sum_price += price

            print(f"{gb_var.Name()} Gbs: {solver.Value(gb_var)}, Price {price}")

        print(f"Sum cloud storage {sum_gbs}")
        print(f"Sum price {sum_price}")


if __name__ == '__main__':
    solve_cloud()
